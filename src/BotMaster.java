import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.NoSuchElementException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.DataOutputStream;
import java.io.BufferedOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import javax.persistence.*;
import javax.persistence.criteria.*;

import domain.Configuration;
import domain.QueueEntry;
import model.QueueEntry_;

public class BotMaster {

	private static domain.Configuration cfg;

	// system data
	public static List <String> queue;
	public static int qhead;

	private static ThreadGroup runners;
	private static EntityManager em;
	public static EntityManagerFactory emf;

	private static final void sysinit()
	{
		// sysinit
		runners=new ThreadGroup("Crawler-workers");
		emf = Persistence.createEntityManagerFactory("FTP Crawler", System.getProperties());
		em = emf.createEntityManager();

		queue=new ArrayList <String>();
		qhead=-1;
	}

	public static final void main(String argv[])
	{
		sysinit();
		configure();
		watchQueue();
	}

	/** Load configuration from database and
	 *  create directories for storing data
	 */
	private static final void configure()
	{
		cfg = em.find(Configuration.class, 1);
		if ( cfg == null ) {
			cfg = new Configuration();
			cfg.defaultconfiguration();
			cfg.setId((byte) 1);
			em.getTransaction().begin();
			em.persist(cfg);
			em.getTransaction().commit();
		}

		new File(cfg.getDownload_dir()).mkdirs();
		new File(cfg.getTmp_dir()).mkdirs();
	}

	private static final void watchQueue()
	{
		boolean empty=false;
		long timerstart=System.currentTimeMillis();

		while(true)
		{
			configure();
			parseQueue();

			if(!startThreads())
			{
				/* empty queue */
				if(cfg.getExitonempty()>0)
				{
					if(System.currentTimeMillis()-timerstart>cfg.getExitonempty()*1000L)
					{
						log_write(cfg.getLog(),"Ending operation, because queue is empty.");
						return;
					}
				}

				if(empty==false)
				{
					log_write(cfg.getLog(),"Crawler Bot Master is idle.");
					if(cfg.getExitonempty()>0)
						System.out.print(" will stop after "+cfg.getExitonempty()+"s.");
					empty=true;
				}
			}
			else
			{
				timerstart=System.currentTimeMillis();
				empty=false;
			}

			try
			{
				Thread.sleep(cfg.getQchecktime()*1000L);
			}
			catch(InterruptedException intr)
			{
				log_write(cfg.getLog_err(),"[!!!] GOT INTR, QueueWatch ended.");
				break;
			}
		}
	} /* watchQueue */

	/**
	 * loads queue from database and merges is with current queue
	 */
	private final static void parseQueue()
	{
		List <String> oldqueue;

		/* create copy of oldqueue */
		oldqueue=new ArrayList <String>(queue);

		/* load new queue from database using criteria query API */
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<String> cq = cb.createQuery(String.class);
		Root <QueueEntry> root = cq.from(QueueEntry.class);
		cq.select(root.get(QueueEntry_.URL));
		cq.orderBy(cb.asc(root.get(QueueEntry_.URL)));

		List <String> newqueue = em.createQuery(cq).getResultList();

		for ( String line:newqueue )
			{
				if(line == null) continue;

				// remove whitespace from queue entry
				try
				{
					StringTokenizer st;
					st=new StringTokenizer(line);
					line=st.nextToken();
				}
				catch(NoSuchElementException e)
				{
					continue;
				}

				try
				{
					/* test if url is valid */
					String url;
					url=line.trim().toLowerCase();
					new URL(url);
					if( ! queue.contains(url) )
					{
						log_write(cfg.getLog(),"[+++] Server "+url+" added.");
						queue.add(url);
					}
					else
					{
						/* we already have this url, remove it from to be deleted list */
						oldqueue.remove(url);
					}
				}
				catch (MalformedURLException grr)
				{
					log_write(cfg.getLog_err(),"[!!!] Bad queue URL="+line);
					continue;
				}
			}

		/* remove stale queue entries */
		for(String url:oldqueue)
		{
			queue.remove(url);
			log_write(cfg.getLog(),"[---] Server "+url+" removed.");
		}
	}

	/* vraci true pokud je aktivni */
	private final static synchronized boolean startThreads()
	{
		int ac;
		boolean active;
		long now;
		boolean rotated;

		active=false;
		rotated=false;
		ac=runners.activeCount();
		now=System.currentTimeMillis();

		if(ac>0) active=true;
		if(ac>=cfg.getThreads()) return true;

		int qsize=queue.size();
		if(qsize==0) return active; // empty queue
		qloop:while(true)
		{
			qhead++;
			if(qhead>=qsize)
			{
				if(rotated) return active;
				else
				{
					qhead=0;
					rotated=true;
				}
			}

			URL qs;
			File lf;
			try {
				qs=new URL((String)queue.get(qhead));
			}
			catch (MalformedURLException ddt) { continue;}

			/* check for downloaded data */
			for(String ext:cfg.getExts())
			{
				lf=new File(cfg.getDownload_dir()+'/'+qs.getHost()+ext);
				if(lf.exists() && lf.lastModified()+cfg.getSiterescantimeout()>now)
				{
					// System.out.println("siterescan not reached for "+lf);
					continue qloop;
				}
			}
			/* check if another download from same site is active */
			lf=new File(cfg.getTmp_dir()+'/'+qs.getHost());
			if(lf.exists() && lf.lastModified()+cfg.getSitelocktimeout()>now)
			{
				// System.out.println("tmp file is active for "+lf);
				active=true;
				continue;
			}

			// start it up!
			try
			{
				Thread t;

				t=new Thread(runners, new FTPCrawler(qs,new DataOutputStream(new BufferedOutputStream(new FileOutputStream(lf))),cfg));
				t.start();
				Thread.yield();
				ac++;
				active=true;
				log_write(cfg.getLog(),"[000] Crawler started for "+qs);
			}
			catch (IOException ignore) {log_write(cfg.getLog_err(),"Error starting crawler for "+qs);}
			if(ac>=cfg.getThreads()) return true;
		}
	}

	/* ------------- low level stuff ------------- */
	public final static synchronized void log_write(String fname,String msg)
	{
		System.out.println(new Date().toString()+" "+msg);
		if(fname==null) return;
		try
		{
			DataOutputStream dos=new DataOutputStream(new BufferedOutputStream(new FileOutputStream(fname,true)));
			dos.writeBytes(new Date().toString());
			dos.writeBytes(" ");
			dos.writeBytes(msg);
			dos.writeBytes("\n");
			dos.close();
		}
		catch (IOException drunk) {}
	}
}
