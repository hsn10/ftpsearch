package domain;

import java.util.ArrayList;
import java.util.List;
import java.io.File;

import javax.persistence.*;

/**
 * Class for storing botmaster robot configuration
 *
 * @author Radim Kolar
 */

@Entity
@Table(name="config")
public class Configuration {

	/** Configuration id */
	@Id
	byte id;

	@Version
	long version;

	/** Configuration name */
	String comment;

	/** file extensions for downloaded lists */
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="ext")
	List <String> exts;

	/** can stop robot on empty queue after desired number of seconds */
	int exitonempty;

	/** use active FTP connections */
	boolean nopasv;

	/** where to put completed file lists */
	@Basic(optional=false)
	@Column(nullable=false)
	String download_dir;

	/** where to put temporary file lists */
	@Basic(optional=false)
	@Column(nullable=false)
	String tmp_dir;

	/** number of threads to use for downloading from FTP sites */
	short threads;

	/* - retry configuration - */

	/** how many retries to one FTP site before giving up */
	short retry;
	/** how many seconds wait after connection failure before retry */
	int retrytimeout;
	/** how many seconds add to retry timeout at each retry */
	int retryinc;
	/** socket read timeout */
	int timeout;

	/** how long is tmp file considered active in ms */
	long sitelocktimeout;
	/** minimum time between FTP site rescan in ms */
	long siterescantimeout;

	/* how often check queue for new entries */
	int qchecktime;

	/* log setup */

	/** normal log file */
	String log;
	/** error log file */
	String log_err;

	public void defaultconfiguration() {
		/* init to sane defaults */
		exitonempty=0;
		download_dir="data";
		tmp_dir="tmp";
		threads=15; retry=15;
		timeout=60*4;
		retry=20;
		retrytimeout=60;
		retryinc=30;
		qchecktime=60;
		sitelocktimeout=4*60*60*1000L; /* 4 hours */
		siterescantimeout=31*24*60*60*1000L; /* 1 month */

		log=null;
		log_err=null;

		exts = new ArrayList <String>();
		exts.add("");
		exts.add(".gz");
		exts.add(".bz2");
	}

	public Configuration() {

	}

	private final static String kill_end_slash(String str)
	{
		if(str.endsWith(File.separator)) str=str.substring(0,str.length()-1);
		return str;
	}

	/**
	 * @param downloadDir the download_dir to set
	 */
	public void setDownload_dir(String downloadDir) {
		download_dir = kill_end_slash(downloadDir);
	}

	/**
	 * @param tmpDir the tmp_dir to set
	 */
	public void setTmp_dir(String tmpDir) {
		tmp_dir = kill_end_slash(tmpDir);
	}


	/* ------ autogen get/set ------- */

	/**
	 * @return the id
	 */
	public byte getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(byte id) {
		this.id = id;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the exts
	 */
	public List<String> getExts() {
		return exts;
	}

	/**
	 * @param exts the exts to set
	 */
	public void setExts(List<String> exts) {
		this.exts = exts;
	}

	/**
	 * @return the exitonempty
	 */
	public int getExitonempty() {
		return exitonempty;
	}

	/**
	 * @param exitonempty the exitonempty to set
	 */
	public void setExitonempty(int exitonempty) {
		this.exitonempty = exitonempty;
	}

	/**
	 * @return the nopasv
	 */
	public boolean isNopasv() {
		return nopasv;
	}

	/**
	 * @param nopasv the nopasv to set
	 */
	public void setNopasv(boolean nopasv) {
		this.nopasv = nopasv;
	}

	/**
	 * @return the download_dir
	 */
	public String getDownload_dir() {
		return download_dir;
	}


	/**
	 * @return the tmp_dir
	 */
	public String getTmp_dir() {
		return tmp_dir;
	}

	/**
	 * @return the threads
	 */
	public short getThreads() {
		return threads;
	}

	/**
	 * @param threads the threads to set
	 */
	public void setThreads(short threads) {
		this.threads = threads;
	}

	/**
	 * @return the retry
	 */
	public short getRetry() {
		return retry;
	}

	/**
	 * @param retry the retry to set
	 */
	public void setRetry(short retry) {
		this.retry = retry;
	}

	/**
	 * @return the retrytimeout
	 */
	public int getRetrytimeout() {
		return retrytimeout;
	}

	/**
	 * @param retrytimeout the retrytimeout to set
	 */
	public void setRetrytimeout(int retrytimeout) {
		this.retrytimeout = retrytimeout;
	}

	/**
	 * @return the retryinc
	 */
	public int getRetryinc() {
		return retryinc;
	}

	/**
	 * @param retryinc the retryinc to set
	 */
	public void setRetryinc(int retryinc) {
		this.retryinc = retryinc;
	}

	/**
	 * @return the timeout
	 */
	public int getTimeout() {
		return timeout;
	}

	/**
	 * @param timeout the timeout to set
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	/**
	 * @return the sitelocktimeout
	 */
	public long getSitelocktimeout() {
		return sitelocktimeout;
	}

	/**
	 * @param sitelocktimeout the sitelocktimeout to set
	 */
	public void setSitelocktimeout(long sitelocktimeout) {
		this.sitelocktimeout = sitelocktimeout;
	}

	/**
	 * @return the siterescantimeout
	 */
	public long getSiterescantimeout() {
		return siterescantimeout;
	}

	/**
	 * @param siterescantimeout the siterescantimeout to set
	 */
	public void setSiterescantimeout(long siterescantimeout) {
		this.siterescantimeout = siterescantimeout;
	}

	/**
	 * @return the qchecktime
	 */
	public int getQchecktime() {
		return qchecktime;
	}

	/**
	 * @param qchecktime the qchecktime to set
	 */
	public void setQchecktime(int qchecktime) {
		this.qchecktime = qchecktime;
	}

	/**
	 * @return the log
	 */
	public String getLog() {
		return log;
	}

	/**
	 * @param log the log to set
	 */
	public void setLog(String log) {
		this.log = log;
	}

	/**
	 * @return the log_err
	 */
	public String getLog_err() {
		return log_err;
	}

	/**
	 * @param logErr the log_err to set
	 */
	public void setLog_err(String logErr) {
		log_err = logErr;
	}

	public long getVersion() {
		return version;
	}
}