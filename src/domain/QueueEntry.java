package domain;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: QueueEntry
 */
@Entity
@Table(name="queue")

public class QueueEntry {

	/** queue entry id */
	@Id
	@GeneratedValue
	int id;

	/** URL to be placed in queue */
	@Basic(optional=false)
	@Column(nullable=false,unique=true)
	String URL;

	@Version
	long version;

	public QueueEntry() {
		super();
	}

	public QueueEntry(String URL) {
		super();
		setURL(URL);
	}

	public String getURL() {
		return this.URL;
	}

	public void setURL(String URL) {
		this.URL = URL;
	}

	public int getId() {
		return this.id;
	}

	public long getVersion() {
		return version;
	}
}
