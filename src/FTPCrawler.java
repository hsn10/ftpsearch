import java.net.URL;
import java.net.Socket;
import java.net.ServerSocket;
import java.net.InetAddress;
import java.io.EOFException;
import java.io.IOException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import domain.Configuration;

/**
 * Crawler for single FTP site
 */

public class FTPCrawler  implements Runnable {

	protected Configuration cfg;

	/* arguments */
	protected DataOutputStream out;
	protected URL url;

	/* inside */
	protected DataInputStream is;
	protected DataOutputStream os;
	protected String cwd;
	protected DataInputStream datain;
	protected Socket s;
	protected String type;
	protected int    ltype;

	public FTPCrawler (URL start, DataOutputStream os, Configuration conf)
	{
		this.out=os;
		this.url=start;
		this.cwd="";
		this.ltype=0;
		this.cfg=conf;
	}

	protected void login() throws IOException
	{
		int rc;
		String auth;

		// 1. connect to FTP server
		s=new Socket(url.getHost(),url.getPort()==-1? url.getDefaultPort(): url.getPort());
		s.setSoTimeout(cfg.getTimeout()*1000);
		is=new DataInputStream(new BufferedInputStream(s.getInputStream(),4096));
		os=new DataOutputStream(new BufferedOutputStream(s.getOutputStream(),4096));
		if(getResponse()>299)
		{
			close();
			throw new EOFException("No or bad welcome banner");
		}

		// 2. login
		auth=url.getUserInfo();

		if(auth==null)
		{
			sendCommand("USER anonymous");
			rc=getResponse();
			if(rc>=400)
			{
				sendCommand("USER ftp");
				rc=getResponse();
			}
		} else
		{
			rc=auth.indexOf(':');
			if(rc == -1)
				rc=auth.length();
			sendCommand("USER "+auth.substring(0,rc));
			/* cut username from auth string */
			if(rc != auth.length())
				auth=auth.substring(rc+1);
			else
				auth=null;

			rc=getResponse();
		}
		if(rc>=400)
			throw new EOFException("Login failed (username)");

		if(rc>=300 && rc<400)
		{
			if(auth==null || auth.length() == 0)
				sendCommand("PASS crawler@filez.com");
			else
				sendCommand("PASS "+auth);
			rc=getResponse();
			if(rc>=300)
				throw new EOFException("Login failed (password)");
		}
		sendCommand("TYPE A");
		getResponse();
		cwd="";
	}

	protected void open() throws IOException
	{
		String sdir;

		login();

		sdir=url.getPath();
		if(sdir==null || sdir.length()==0)
			sdir="/";

		if(!cd(sdir))
			throw new EOFException("CD to starting point failed.");

		autodetect();
	}

	private void autodetect() throws IOException
	{
		int l8;
		int l7;
		int l3;
		String line;
		StringTokenizer st;

		/* get system type */
		type=null;
		sendCommand("SYST");
		type=getReply();
		if(type.startsWith("215 "))
			type=type.substring(4);
		else
			type=null;

		/* try to detect listing format */
		l8=0;
		l7=0;
		l3=0;
		FTPlistDirectory("/");

		while(true)
		{
			line=datain.readLine();
			if(line==null)
			{
				/* EOF */
				datain.close();
				datain=null;
				getResponse();
				break;
			}

			st=new StringTokenizer(line);
			if(st.countTokens()==4) l3++;
			else
			{
				/* bereme jen soubory a adresare, kvuli spolehlivejsi autodetekci */
				if(!line.startsWith("d") && !line.startsWith("-") )
					continue;
				if(st.countTokens()==9) l8++;
				if(st.countTokens()==8) l7++;
			}
		}
		if(l3 > Math.max(l8,l7)) ltype=3;
		else
			if(l8>l7) ltype=8;
			else
				if(l7>l8) ltype=7;
				else
				{
					BotMaster.log_write(cfg.getLog_err(),"Listing format autodetect failed on "+url.getHost()+" Server reports system type "+type);
					ltype=8; /* more common */
				}
	}

	public void close() throws IOException
	{
		if(s != null)
			s.close();
		s=null;
		datain=null;
		is=null;
		os=null;
		cwd="";
	}

	public void crawl() throws IOException
	{
		crawldir(cwd);
	}

	public boolean cd(String path) throws IOException
	{
		String pwd;

		if(path.endsWith("/") && path.length()>1)
			path=path.substring(0,path.length()-1);

		if(cwd.equals(path))
			return true;

		sendCommand("CWD "+path);
		if(getResponse()>=400)
			return false;

		sendCommand("PWD");
		pwd=getReply();
		if(pwd.startsWith("257 "))
		{
			int i,j;
			i=pwd.indexOf('"');
			j=pwd.lastIndexOf('"');
			if(j>i) {
				pwd=pwd.substring(i+1,j);
				pwd=pwd.replaceAll("\"\"","\"");

				if(path.startsWith("/") && pwd.equals(path))
				{
					cwd=pwd;
					return true;
				} else
					if( ( (cwd.length()>1? cwd: "")+"/"+path).equals(pwd) )
					{
						cwd=pwd;
						return true;
					} else
					{
						//	  System.err.println("pwd check failed. wanted="+path+" from cwd="+cwd+" got pwd="+pwd );
					}
			}
		} else
			System.err.println("unexpected reply to pwd:"+pwd);

		/* go back to original directory and exit */
		sendCommand("CWD "+cwd);
		getResponse();
		return false;
	}

	/* ============ low level code ================ */

	protected void crawldir(String startpoint) throws IOException
	{
		List <String> subdirs;
		String mydir;
		int retry;

		if(!cd(startpoint))
		{
			return;
		}

		subdirs=new ArrayList<String>();
		mydir=cwd;
		retry=0;

		while(true)
		{
			try
			{
				FTPlistDirectory(null);
				FTPparseDirectory(subdirs);
			}
			catch (java.io.InterruptedIOException ohmy)
			{
				retry++;
				System.err.println(
						"IO error on DATA connection to "+url.getHost()+", retry "+retry);
				if(retry<cfg.getRetry())
				{
					try{
						Thread.sleep(1000L*(cfg.getRetrytimeout()+retry*cfg.getRetryinc()));
					}
					catch (InterruptedException iz) {}
					continue;
				}
				else
					throw new EOFException("Too many retries");
			}
			break;
		}

		for (String nextdir: subdirs)
		{
			retry=0;
			while(true)
			{
				if(retry>0) login();
				cd(mydir);
				try
				{
					crawldir(nextdir);
				}
				catch (EOFException err)
				{
					throw err;
				}
				catch (IOException err2)
				{
					retry++;
					System.err.println("IO error on CONTROL connection to "+url.getHost()+", retry "+retry);
					if(retry<cfg.getRetry())
					{
						try {
							Thread.sleep(1000L*(cfg.getRetrytimeout()+retry*cfg.getRetryinc()));
						}
						catch (InterruptedException irrz) {}
						continue;
					}
					else
						throw new EOFException("Too many retries");
				}
				break;
			}
		}
	}

	protected void FTPparseDirectory(List <String> subdirs) throws IOException
	{
		String line;
		String fsize;
		String ftype;
		String fname;
		StringTokenizer st;
		URL nf;

		if(!type.startsWith("UNIX Type: L8") &&
				!type.startsWith("Windows_NT")
		)
			throw new EOFException("Don't know how to parse system: "+type);

		/* directory listing error */
		if(datain == null) return;

		try
		{
			while(true)
			{
				line=datain.readLine();
				if(line==null)
				{
					/* EOF */
					datain.close();
					datain=null;
					getResponse();
					break;
				}
				if(line.startsWith("total "))
					continue;
				st=new StringTokenizer(line);
				if(st.countTokens()<ltype+1)
				{
					System.err.println("too short line: "+line);
					continue;
				}

				if(ltype==3)
				{
					/* windows, ms-dos style listing */
					st.nextToken(); /* date  i.e. 07-03-04 */
					st.nextToken(); /* time  12:51AM */
					ftype=st.nextToken(); /* filesize or <DIR> */
					fname=st.nextToken("").trim();
					if (ftype.equals("<DIR>"))
					{
						subdirs.add(fname);
						continue;
					}
					else
					{
						fsize=ftype;
					}
				}
				else
				{
					/* UNIX-WORLD */
					ftype=st.nextToken();
					/* bereme jen soubory a adresare */
					if(!ftype.startsWith("d") && !ftype.startsWith("-") )
						continue;
					st.nextToken(); /* nlinks */
					st.nextToken(); /* owner  */
					if(ltype==8) st.nextToken(); /* group  */
					fsize=st.nextToken();
					st.nextToken(); /* month */
					st.nextToken(); /* day   */
					st.nextToken(); /* hour/year */
					fname=st.nextToken("").trim();
					if (ftype.startsWith("d"))
					{
						if(fname.equals(".") || fname.equals(".."))
							continue;
						subdirs.add(fname);
						continue;
					}
				}
				nf=new URL(this.url,(cwd.length()>1? cwd : "") +"/"+fname);
				out.writeBytes(nf+"\t"+fsize+'\n');
			}
		}
		catch (IOException achjo)
		{
			throw new java.io.InterruptedIOException();
		}
	}

	/* puts data into datain class var. */
	protected final void FTPlistDirectory(String dirname) throws IOException
	{
		// data socket
		Socket data=null;
		ServerSocket portdata=null;
		datain=null;
		int rc;
		String line;

		/* establish a data connection */
		/* 1. PASV command */
		if(cfg.isNopasv()==true)
			line="NOPASV";
		else
		{
			sendCommand("PASV");
			line=getReply();
		}

		if(line.startsWith("227 "))
		{
			int l;
			int r;

			l=line.lastIndexOf('(');
			r=line.lastIndexOf(')');

			if(l>=4 && r>=16 && r>l )
			{
				StringTokenizer st=new StringTokenizer(line.substring(l+1,r),", ");
				if(st.countTokens()==6)
				{
					String ha="";
					for(int i=1;i<=3;i++)
					{
						ha+=st.nextToken()+".";
					}
					ha+=st.nextToken();
					try
					{
						data=new Socket(InetAddress.getByName(ha),Integer.valueOf(st.nextToken()).intValue()*256+Integer.valueOf(st.nextToken()).intValue());
						data.setSoTimeout(cfg.getTimeout()*1000);
					}
					catch (IOException ajajaj)
					{
						// System.err.println("connect to server "+url.getHost()+" failed");
						sendCommand("ABOR");
						getResponse();
						data=null;
						throw new java.io.InterruptedIOException();
					}
				}
			} else throw new EOFException("Malformed PASV reply: "+line);
		} else
			if(cfg.isNopasv()==false)
				throw new java.io.InterruptedIOException();

		if(data==null)
		{
			/* port mode */
			portdata=new ServerSocket(0);
			InetAddress me=s.getLocalAddress();
			int p1=portdata.getLocalPort()/256;
			int p2=portdata.getLocalPort()-256*p1;
			sendCommand("PORT "+me.getHostAddress().replace('.',',')+","+p1+","+p2);
			p2=getResponse();
			if(p2>300 || p2<200) {
				throw new EOFException("PORT COMMAND failed. rc="+p2);
			}
		}

		/* 3. LIST COMMAND */
		sendCommand("LIST -a" + (dirname == null? "" : " "+dirname) );
		rc=getResponse();
		if(rc>299)
		{
			if(data!=null) data.close();
			if(portdata!=null) portdata.close();
			return;
		}

		/* OPEN DATAINPUT CONNECTION */
		if(portdata!=null) {
			/* listen on socket */
			data=portdata.accept();
			portdata.close();
		}

		if(data!=null)
			datain=new DataInputStream(new BufferedInputStream(data.getInputStream(),8192));
	}

	private final int getResponse() throws IOException
	{
		String line;
		String code=null;
		int err;

		while(true)
		{

			line=is.readLine();
			if(line==null) throw new IOException();
			if(code!=null)
				if(!line.startsWith(code)) continue;
			if(line.length()<4)
			{
				// System.err.println("Too short FTP reply: "+line+"/"+code);
				throw new IOException();
			}
			if(line.charAt(3)=='-') { code=line.substring(0,3)+" ";continue; }
			try
			{
				err=Integer.valueOf(line.substring(0,3)).intValue();
			}
			catch (Exception e) { throw new EOFException(e.toString());}
			return err;
		}
	}

	private final String getReply() throws IOException
	{
		String line;

		line=is.readLine();
		if(line==null) throw new java.io.IOException("control connection closed");
		return line;
	}

	private final void sendCommand(String command) throws IOException
	{
		os.writeBytes(command+"\r\n");
		os.flush();
	}

	public void run()
	{
		try
		{
			this.open();
			this.crawl();
			this.close();
			this.out.close();
			new File(cfg.getTmp_dir()+"/"+url.getHost()).renameTo(new File(cfg.getDownload_dir()+"/"+url.getHost()));
		}
		catch (IOException sorry) {}
	}

	/* ------------ main --------------- */
	public static void main(String argv[]) throws IOException
	{
		FTPCrawler me;
		DataOutputStream out;
		URL target;
		Configuration cfg;

		target=new URL(argv[0]);
		cfg = new Configuration();
		cfg.defaultconfiguration();
		new File("tmp").mkdirs();
		new File("data").mkdirs();
		out=new DataOutputStream(new BufferedOutputStream(new FileOutputStream("tmp/"+target.getHost())));

		me=new FTPCrawler(target,out,cfg);
		me.open();
		me.crawl();
		me.close();
		out.close();
		new File("tmp/"+target.getHost()).renameTo(new File("data/"+target.getHost()));
	}
}
